#!/bin/sh

##########################
# MAKE ORB-SLAM2 DATASET #
##########################

# -------------------------------------------------------------------- |
#                       SCRIPT OPTIONS                                 |
# ---------------------------------------------------------------------|
RAW_DATA_NAME=$1
SAVE_NAME=$2       
FPS=$3
# -------------------------------------------------------------------- |


python make_dataset.py --movie_path=../dataset/raw_data/${RAW_DATA_NAME} --save_path=../dataset/slam_data/${SAVE_NAME} --fps=${FPS}
