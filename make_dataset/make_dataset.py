import cv2
import numpy as np
import os
import getopt
import sys



def resize(img, height, width):
    size = (height, width)
    resized_img = cv2.resize(img, size)
    return resized_img

def make_dataset(video_path, save_path, fps=30):
    num = 1e10
    if not os.path.exists(os.path.join(save_path, 'rgb')): os.makedirs(os.path.join(save_path, 'rgb'))
    cap = cv2.VideoCapture(video_path)

    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret == True:
            # transposed_frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
            transposed_frame = frame
            # resized_frame = resize(transposed_frame, 360, 640)
            resized_frame = transposed_frame
            cv2.imwrite(os.path.join(save_path, "rgb/{0:04f}".format(num/fps)+".png"),resized_frame)
            with open(os.path.join(save_path, "rgb.txt"), mode='a') as fw:
                fw.write("{0:04f} rgb/{0:04f}.png\n".format(num/fps, num/fps))
            print(os.path.join(save_path, "rgb/{0:04f}".format(num/fps)+".png"))
            num += 1
        else:
            break

    cap.release()

def main():
    # args
    args, img_mask = getopt.getopt(sys.argv[1:], '', ['movie_path=', 'save_path=', 'fps='])
    args = dict(args)

    movie_path = args.get('--movie_path')
    save_path = args.get('--save_path')
    fps = int(args.get('--fps'))

    make_dataset(movie_path, save_path, fps)

if __name__ == '__main__':
    main()




