#!/bin/sh

############################
# MAKE ORB-SLAM2 YAML FILE #
############################

# -------------------------------------------------------------------- |
#                       SCRIPT OPTIONS                                 |
# ---------------------------------------------------------------------|
MOVIE_NAME=$1
YAML_NAME=$2
SQUARE_SIZE=$3
CHEST_BOARD_HEIGHT=$4
CHEST_BOARD_WIDTH=$5
FPS=$6
RGB=$7
# -------------------------------------------------------------------- |


# movie to image
python convert_movie2image.py --movie=../datasets/calib_data/movie/${MOVIE_NAME} --image=../datasets/calib_data/image

# calibrate
python adjust_cam_params.py --image_path=../datasets/calib_data/image --yaml_path=../yaml/${YAML_NAME} --square_size=${SQUARE_SIZE} --cb_H=${CHEST_BOARD_HEIGHT} --cb_W=${CHEST_BOARD_WIDTH} --fps=${FPS} --rgb=${RGB}

# delete image
rm ../datasets/calib_data/image/*