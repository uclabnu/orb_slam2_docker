import cv2
import getopt
import sys
import yaml
import os

def main():
    # args
    args, img_mask = getopt.getopt(sys.argv[1:], '', ['movie=', 'image='])
    args = dict(args)
    args.setdefault('--movie', './movie')
    args.setdefault('--image', './image')

    video_path = args.get('--movie')
    cap = cv2.VideoCapture(video_path)

    image_path = os.path.join(args.get('--image'), '*')
    num = 0
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret == True:
            if num % 10 == 0:
                cv2.imwrite(os.path.join(args.get('--image'), "img{:0=3}".format(num)+".jpg"),frame)
                print("save img{:0=3}".format(num)+".jpg")
            num += 1
        else:
            break

    cap.release()

if __name__ == '__main__':
    main()