import numpy as np
import cv2
import glob
import sys
import getopt
import yaml
import os
from tqdm import tqdm


def read_yaml(path):
    f = open(path, 'r')
    data = yaml.load(f)
    return data

def write_yaml(path, data):
    f = open(path, "w")
    f.write(yaml.dump(data, default_flow_style=False))
    f.close()

def save_camera_setting(path, camera_matrix, dist_coefs, fps, rgb):
    config = read_yaml('./template.yaml')

    config['Camera.fx'] = float(camera_matrix[0][0])
    config['Camera.fy'] = float(camera_matrix[1][1])
    config['Camera.cx'] = float(camera_matrix[0][2])
    config['Camera.cy'] = float(camera_matrix[1][2])

    config['Camera.k1'] = float(dist_coefs[0])
    config['Camera.k2'] = float(dist_coefs[1])
    config['Camera.p1'] = float(dist_coefs[2])
    config['Camera.p2'] = float(dist_coefs[3])
    config['Camera.k3'] = float(dist_coefs[4])

    config['Camera.fps'] = fps
    config['Camera.RGB'] = rgb

    write_yaml(path, config)

def calibrate(image_path, square_size, cb_H, cb_W):
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, square_size, 0.001)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((cb_H*cb_W,3), np.float32)
    objp[:,:2] = np.mgrid[0:cb_H,0:cb_W].T.reshape(-1,2)

    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.

    images = glob.glob(os.path.join(image_path, '*'))

    for fname in tqdm(images):
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (cb_H,cb_W),None)

        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)

            corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
            imgpoints.append(corners2)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
    return ret, mtx, dist, rvecs, tvecs

def main():
    # args
    args, img_mask = getopt.getopt(sys.argv[1:], '', ['image_path=', 'yaml_path=', 'square_size=', 'cb_H=', 'cb_W=', 'fps=', 'rgb='])
    args = dict(args)
    args.setdefault('--image_path', './image')
    args.setdefault('--yaml_path', './')
    args.setdefault('--square_size', 20)
    args.setdefault('--cb_H', 7)
    args.setdefault('--cb_W', 9)
    args.setdefault('--fps', 30)
    args.setdefault('--rgb', 1)

    # chest board params
    square_size = int(args.get('--square_size'))
    cb_H = int(args.get('--cb_H'))
    cb_W = int(args.get('--cb_W'))

    # data path
    image_path = args.get('--image_path')
    yaml_path = args.get('--yaml_path')

    # fps & rgb setting
    fps = int(args.get('--fps'))
    rgb = int(args.get('--rgb'))

    # calibrate
    _, mtx, dist, _, _ = calibrate(image_path, square_size, cb_H, cb_W)
    
    # save yaml file
    save_camera_setting(yaml_path, mtx, dist.ravel(), fps, rgb)


if __name__ == '__main__':
    main()