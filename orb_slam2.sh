#!/bin/sh
echo $DISPLAY

if [ $3 = "true" ]; then
    # roscore
    docker run -d --rm \
        -e DISPLAY=$DISPLAY \
        -e PATH=$PATH:/opt/ros/kinetic/bin \
        --net host \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v $HOME/.Xauthority:/root/.Xauthority \
        orb-slam2 \
        /bin/bash -c 'source /opt/ros/melodic/setup.bash; roscore'

    # rosrun
    docker run -d --rm \
        -e DISPLAY=$DISPLAY \
        -e PATH=$PATH:/opt/ros/kinetic/bin \
        --net host \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v $HOME/.Xauthority:/root/.Xauthority \
        orb-slam2 \
        /bin/bash -c "source /opt/ros/melodic/setup.bash; ROS_PACKAGE_PATH=/opt/catkin_ws/src/ORB_SLAM2/Examples/ROS:/opt/ros/melodic/share; rosrun ORB_SLAM2 Mono Vocabulary/ORBvoc.txt ../../../$1"
fi

# send data
docker run -it --rm \
    -e DISPLAY=$DISPLAY \
    -e PATH=$PATH:/opt/ros/kinetic/bin \
    --net host \
    -v $HOME/ORB-SLAM2/dataset:/opt/dataset \
    -v $HOME/ORB-SLAM2/yaml:/opt/yaml  \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $HOME/.Xauthority:/root/.Xauthority \
    orb-slam2 \
    /bin/bash -c "./Examples/Monocular/mono_tum Vocabulary/ORBvoc.txt ../../../$1 ../../../$2 $3"



