# ORB_SLAM2_Dokcer
本家: [ORB-SLAM2](https://github.com/raulmur/ORB_SLAM2)
## SLAM実行用のyamlファイルの作成
checker-boardの用意
- `make_yaml`下の`camera-calibration-checker-board_9x7.pdf`を倍率100%で印刷する（別途用意していただいてもかまいません）
カメラキャリブレーション用の動画を撮影する
- SLAMで用いるカメラを用意する (例: iPhone11)
- 用意したchecker-boardを様々な角度、位置から撮影する
- `dataset/calib_data/movie`下にカメラのキャリブレーション用に撮影したチェスボードの動画を配置

yamlファイルを生成する
```bash
cd make_yaml
sh ./make_yaml.sh [MOVIE_NAME] [YAML_NAME] [SQUARE_SIZE] [CHEST_BOARD_HEIGHT] [CHEST_BOARD_WIDTH] [FPS] [RGB]
```
- MOVIE_FILE
    - `dataset/calib_data/movie`下に配置した動画のファイル名（例: iphone11.MOV）
- YAML_FILE
    - 保存するyamlのファイル名（例: iphone11.yaml）
- SQUARE_SIZE
    - checker-boardの1マスの辺の長さ[mm]（例: 20）
- CHEST_BOARD_HEIGHT
    - checker-boardの縦のマス目の数（例: 7）
- CHEST_BOARD_WIDTH
    - checker-boardの横のマス目の数（例: 9）
- FPS
    - カメラで撮影する動画のfps（例: 30）
- RGB
    - 0: BGR
    - 1: RGB

`yaml`下にyamlファイルが生成される

## SLAM用のデータの生成（単眼RGB）
`datasets/raw_data`下にSLAMを実行したい動画ファイルを配置

動画ファイルをSLAM用データに変換する
```
cd make_yaml
sh ./make_yaml.sh [RAW_DATA] [SLAM_DATA_DIR] [FPS]
```
- RAW_DATA
    - 動画ファイル
- SLAM_DATA_DIR
    - SLAM用データを保存するディレクトリの名前
- FPS
    - カメラで撮影した動画のfps

`dataset/slam_data`下にデータセットが生成される

## SLAMを実行
Dockerイメージを作成
```bash
docker image build -t orb-slam2 .
```
SLAMを実行
```bash
sh orb_slam2.sh  yaml/[YAML_FILE] dataset/slam_data/[SLAM_DATA_DIR] [VIS_OPT]
```
- VIS_OPT
    - true: 位置推定の可視化あり
    - false: 位置推定の可視化なし