# FROM nvidia/cuda:10.2-devel-ubuntu18.04
FROM ubuntu:18.04

# timezoneの選択で停止するのを防ぐ
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
  && apt-get install -y git

# 必要なパッケージをインストール
RUN apt-get update \
	&& apt-get install -y lsb-release \
	&& apt-get install -y -my wget gnupg

# ROS
ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' \
	&& apt-key del F42ED6FBAB17C654 \
	&& apt-key adv --keyserver hkp://ipv4.pool.sks-keyservers.net:80 --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 \
	&& apt-get update \
	&& apt-get install -y ros-melodic-desktop-full

RUN apt-get install -y python-rosdep \
	&& rosdep init \
	&& rosdep update \
	&& echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc \
	&& /bin/bash -c "source ~/.bashrc" \
	&& apt-get install -y python-rosinstall python-rosinstall-generator python-wstool build-essential

# OpenCV
WORKDIR /opt/
# RUN apt autoremove -y libopencv-dev python-opencv
RUN wget https://raw.githubusercontent.com/takuto121/milq/master/scripts/bash/install-opencv.sh \
	&& chmod +x install-opencv.sh \
	&& ./install-opencv.sh

# ROS workspace
WORKDIR /opt/catkin_ws/src/
RUN /bin/bash -c "source /opt/ros/melodic/setup.bash && catkin_init_workspace"

WORKDIR /opt/catkin_ws/
RUN /bin/bash -c "source /opt/ros/melodic/setup.bash && catkin_make && source devel/setup.bash"

# # # Pangolin
WORKDIR /opt/catkin_ws/src/
RUN git clone https://github.com/stevenlovegrove/Pangolin.git

WORKDIR /opt/catkin_ws/src/Pangolin/build
RUN apt-get install -y libglew-dev \
	&& cmake .. \
	&& make -j20 \
	&& make install

WORKDIR /opt/catkin_ws/src/
RUN git clone https://github.com/takuto121/ORB_SLAM2.git

WORKDIR /opt/catkin_ws/src/ORB_SLAM2/
RUN chmod +x build.sh \
	&& ./build.sh

RUN chmod +x build_ros.sh \
	&& /bin/bash -c "source /opt/ros/melodic/setup.bash; ROS_PACKAGE_PATH=/opt/catkin_ws/src/ORB_SLAM2/Examples/ROS:/opt/ros/melodic/share; echo $ROS_PACKAGE_PATH; ./build_ros.sh" 
